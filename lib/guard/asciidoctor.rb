require 'guard/compat/plugin'
require 'asciidoctor'
require 'find'

module Guard
  class Asciidoctor < Plugin

    DEFAULTS = {
      names: %w|ad asc adoc asciidoc|,
      document: {},
    }

    # Initializes a Guard plugin.
    # Don't do any work here, especially as Guard plugins get initialized even
    # if they are not in an active group!
    #
    # @param [Hash] options the custom Guard plugin options
    # @option options [Array<Guard::Watcher>] watchers the Guard plugin file watchers
    # @option options [Symbol] group the group this Guard plugin belongs to
    # @option options [Boolean] any_return allow any object to be returned from a watcher
    #
    def initialize(options = {})
      UI.info 'Guard::Asciidoctor initializing'
      @names = options.delete :names
      @names ||= DEFAULTS[:names]
      if @names.is_a? String
        @names = @names.split
      end
      @document = options.delete :document
      @document ||= DEFAULTS[:document]
      ensure_output_directory
      super
    end

    # Called once when Guard starts. Please override initialize method to init
    # stuff.
    #
    # @raise [:task_has_failed] when start has failed
    # @return [Object] the task result
    #
    def start
      UI.info 'Guard::Asciidoctor starting'
      UI.info "Will watch for files ending in #{@names}"
    end

    # Called when `stop|quit|exit|s|q|e + enter` is pressed (when Guard quits).
    #
    # @raise [:task_has_failed] when stop has failed
    # @return [Object] the task result
    #
    # def stop
    #   UI.info 'Guard::Asciidoctor stopping'
    # end

    # Called when `reload|r|z + enter` is pressed.
    # This method should be mainly used for "reload" (really!) actions like reloading passenger/spork/bundler/...
    #
    # @raise [:task_has_failed] when reload has failed
    # @return [Object] the task result
    #
    def reload
      ensure_output_directory
    end

    # Called when just `enter` is pressed
    # This method should be principally used for long action like running all specs/tests/...
    #
    # @raise [:task_has_failed] when run_all has failed
    # @return [Object] the task result
    #
    def run_all
      UI.info 'Guard::Asciidoctor will rebuild everything'
      ensure_output_directory

      # TODO: IMHO there should be a way to know which files I'm being
      # configured to watch for, so I should not iterate on all of them. After a
      # search, this doesn't seem to be the opinion of the Guard developers:
      # https://github.com/guard/guard/issues/466
      files = []
      Find.find(Dir.pwd) do |path|
        if File.file?(path)
          @names.each do |termination|
            files << path if path.end_with?(termination)
          end
        end
      end

      files.each do |path|
        run_one path
      end
    end

    # Called on file(s) additions that the Guard plugin watches.
    #
    # @param [Array<String>] paths the changes files or paths
    # @raise [:task_has_failed] when run_on_additions has failed
    # @return [Object] the task result
    #
    def run_on_additions(paths)
      paths.each do |path|
        run_one path
      end
    end

    # Called on file(s) modifications that the Guard plugin watches.
    #
    # @param [Array<String>] paths the changes files or paths
    # @raise [:task_has_failed] when run_on_modifications has failed
    # @return [Object] the task result
    #
    def run_on_modifications(paths)
      paths.each do |path|
        run_one path
      end
    end

    # Called on file(s) removals that the Guard plugin watches.
    #
    # @param [Array<String>] paths the changes files or paths
    # @raise [:task_has_failed] when run_on_removals has failed
    # @return [Object] the task result
    #
    def run_on_removals(paths)
    end

    private

    def run_one(file_name)
      UI.info "Rendering #{file_name}"
      ::Asciidoctor.render_file(file_name, @document)
    end

    def ensure_output_directory
      if @document.has_key?(:to_dir) && !Dir.exists?(@document[:to_dir])
        Dir.mkdir @document[:to_dir]
      end
    end

  end
end
