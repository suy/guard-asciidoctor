# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'guard/asciidoctor/version'

Gem::Specification.new do |spec|
  spec.name          = "guard-asciidoctor"
  spec.version       = Guard::AsciidoctorVersion::VERSION
  spec.authors       = ["Alejandro Exojo"]
  spec.email         = ["suy@badopi.org"]

  spec.summary       = %q{Guard plugin to automatically render your AsciiDoc documents using Asciidoctor}
  # spec.description   = %q{TODO: Write a longer description or delete this line.}
  # spec.homepage      = "TODO: Put your gem's website or public repo URL here."

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'guard-compat', '~> 1.1'
  spec.add_dependency 'asciidoctor', '~> 1.5.2'

  spec.add_development_dependency "bundler", "~> 1.10"
end
